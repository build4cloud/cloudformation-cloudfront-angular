# Cloudformation Cloudfront Angular
> A Build4Cloud Project

A Cloudformation Template for Angular Apps hosted on Cloudfront.

![Cloudformation Angular Cloudfront](./Cloudfront.png "Angular Cloudfront")

## What you can expect

In this project we will go through the process of creating a Cloudfront hosted Website.
For those of you who are only interested in the template are free to grab the [Code](./templates/cloudfront.cf.json) and continue.
Later we will cover the topics of how to apply this template into your account and what the benefits of using this method are.

## Prerequisite

* Owning a AWS Account
* Owning a Hosted Zone

## Getting started

As you might have noticed we are using [StackMaster](https://github.com/envato/stack_master) for our projects giving us the benefit of [Automation](https://gitlab.com/build4cloud/automateinfrastructurewithcf/blob/master/README.md). If you are not a stack_master you are free to simply paste this template manually into Cloudformation and fill out the parameters. As you might have noticed there are only 3 of them. This template is optimized for Subdomain Websites. Meaning this will not work for Root-Domains like `build4.cloud`. However `www.build4.cloud` is still valid. Ok now we need to get a Certificate for out Domain. Because we are using Cloudfront for Website-Hosting we will need to go into the [Certificate Manager](https://console.aws.amazon.com/acm/home?region=us-east-1#/) and request a certificate for your domain. No matter where you what to deploy your solution, Cloudfront will only accept certificates from `us-east-1` as it is the global entrypoint for AWS. You can still deploy your Cloudformation Stack and S3 Bucket in a different region. Just make sure you request the certificate in `us-east-1`. Ok now we can fill out all parameters and start deploying.

## Deployment with Stack Master

Should you choose to deploy your solution with StackMaster you will need to change to files from this project.
1. [Parameters](./parameters/cloudfront_angular.yml)
2. [StackMaster](./stack_master.yml)

The parameters should be clear by now. You can ignore major parts of the `stack_master.yml` as well. The only important part is the region you want it to be deployed. As a german group we are using the region eu-central-1. As previously mentioned this has close to no impact. The certificate, dns and cloudfront distribution are global (`us-east-1`). To deploy this stack simply use the StackMaster enter the following command:

```shell
stack_master apply
```

For detailed information about StackMaster check our detailed [guide]() about it and the benefits of using it in combination with Gitlab CI.

## Features

### Cost

Now to answer the question about what we actually deployed. This Cloudfront Distribution is a low cost solution to host your website on a global scale. Currently this Content Delivery Network is configured to be cheap and easy to use. AWS provides different [Price](https://aws.amazon.com/cloudfront/pricing/) Classes to configure your Distribution. This template uses the cheapest one, meaning your content is only distributed in the following regions: 
* United States
* Canada
* Europe

The second thing we did was limit the [SSL Certificate](https://aws.amazon.com/cloudfront/custom-ssl-domains/). Meaning this cloudfront distribution does not support [SNI](https://de.wikipedia.org/wiki/Server_Name_Indication). For a private person this would drastically increase the cost of your distribution.

### Angular and Redirects

This tutorial was about [Angular] right? Well yes, but only because it gives us the opportunity to talk about some other Cloudfront features. There are probably more frameworks out there which are using client-side routing in combination with static web content. Angular is just my prime example. Normally your webserver simply returns all requested content as it is on the server. In Angular we have the ability to define routes that the browser interprets generating a more user-friendly experience. So when you are looking for `../category/more/content` but there is no folder Cloudfront would be confused and return an `Error 404`. What we did was redirecting all `404 Errors` to the `index.html`.

### Time to Live

Normally Cloudfront is used to host static content which is not to be changed. However websites change. If we now update our `html.file` we do not want to wait for ages until Cloudfront changes our content. In order to speed up this process we told Cloudfront to update our content immediately. 

### Tweaking the distribution

To get the most out of your distribution you can some settings to better fit your needs. One example of this would be logging and monitoring. Cloudfront has an in-built analytics service. Giving you viewer statistics about your users and their behavior. This includes the most requested sites, devices, browsers, locations, you name it. You can even configure alarms to some of these metrics so you immediately get notified if something is off. If this is not enough or you have your own visualization tool you can even export the access logs to S3 for further processing.

As mentioned earlier you can expand your distribution to further regions if you are have customers in these regions and need shorter latency times. The other major customization part is caching. TTL is only one settings you can set. What requests and how they should be cached can also be configured. Should query strings be cached or only `Get` and `Head` requests. 

## Pros and Cons of using Cloudfront for Website Hosting

### Cost

Cloudfront is not as cheap as it can get because there are many free static website options out there. Compared to renting a Server and setting up an apache or some or webserver it stands no chance. Not only is it easier to simply drop your files into an S3 bucket but you also don't have to worry about security your SSL certificate or even to much load. AWS will handel this for you and compared to free tools it still gives you the advantage of using your own domain and settings. As long as you are using Cloudfront to host simple websites you won't need to worry about being charged much. This is because you will mostly pay for the traffic of you Cloudfront Distribution. Meaning as long as you don't serve large video files you will probably not even notice the cost. One last comparison before wrapping up this topic is `AWS Lambda` There is also the possibility to host your Website on Lambda which is a serverless compute service that runs your code in response to events like web-requests. Lambda is also very cheap and easy to set up. It is somewhere in the middle between Cloudfront and classic server hosting. I will come back to Lambda in the continuing topics so just keep that in mind for now.

### Performance

Nothing beats performance and latency if we are using Cloudfront. Of course there are different cloud-providers that promise even faster serving and lower latency then Cloudfront. These differences are so minimal that it is not worth talking about. If we look at other technologies then Content Delivery Networks you will quickly notice that there is hardly anything that can keep up with them. If you are not keen on hosting you own CDN you will never reach the latency and performance that Cloudfront promises. Simple servers with an nginx might get you nice performance for a small customer base in a specific region but has soon as this changes you will face more and more bottlenecks. With lambda you could easily create a distributed network but it will still take longer than simply serving you website from Cloudfront.

### Server Side Rendering

Because my first of cloudfront was Angular i want to talk about Server Side Rendering and what using Cloudfront is meaning in this context. For those of you who have no idea what I am talking about: Most websites these days use some form of `JavaScript` to render what you see on the screen. Modern Browsers interpret this and we see what a normal webpage. Now try disabling JavaScript in your browser and try viewing some webpages. This is basically how bots see the internet. They do not have a graphical interface and "can't" see a website. (There are some new technologies that are capable of this but they are not being used for web-crawlers.) For angular this is a big problem because everything is happening on your computer and not in the backend. This is where Server Side Rendering comes in play. This technology processes what most of what should have been processed by your computer and servers it to you as a proxy. As you might have guest, this is not static web content anymore and needs computing power. This means that using Cloudfront will get you bad results for anything Search Engine Optimization oriented. Just keep this in mind when using Cloudfront in combination if Angular or any JavaScript Frameworks employing this technology. A solution for this problem would be `AWS Lambda` as it has the advantages of being serverless while still able to do some computing.

## Contributing

If you'd like to contribute, please fork the repository and make changes as
you'd like. Pull requests are warmly welcome.

If your vision of a perfect Cloudfront Distribution greatly from mine, it might be
because your projects are for vastly different. In this case, you can create a
your own project and maybe we can reference each other.

## Related Pages

Here's a list of other related pages where you can find more inspiration for your next project.

- [Build4Cloud](https://build4.cloud)
- [AWS](https://aws.amazon.com)
- [StackMaster](https://github.com/envato/stack_master)
- [Angular](https://angular.io)

## Licensing

MIT License

[Copyright (c) 2019 Build4Cloud](./LICENSE)

## Lastly

Stay tuned for more content.

Patrick Domnick